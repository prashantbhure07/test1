const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code. 
/*
  Complete the following functions.
  These functions only need to work with arrays.
  A few of these functions mimic the behavior of the `Built` in JavaScript Array Methods.
  The idea here is to recreate the functions from scratch BUT if you'd like,
  feel free to Re-use any of your functions you build within your other functions.
  **DONT** Use for example. .forEach() to recreate each, and .map() to recreate map etc.
  You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating
*/

function each(elements, cb) {
  var c=0;
  for(let i=0;i<elements.length;i++)
  {
    cb(elements[i],i,elements)
  }
  }



  function print(elem){
    console.log(c)
  }
  each(items, print) 
  
  function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
      let ar=[]
    for(let i=0;i<elements.length;i++)
    {   
   ​
     ar.push(cb(elements[i],i,elements));
    }
    return ar;
    }
    console.log(map(items,(item)=>item))

  

  
  function reduce(elements, cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.


    
      if(startingValue===undefined)
      {
          index = 1;
          initalValue = elements[0];
      }
      else
  {
      index =0;
      initalValue = startingValue;
  }
    
      let ar=[];
      let c=0;
      for(let i=0;i<elements.length;i++)
      {
        ar.push(elements[i])
        c=c+ar[i]
      }
      return c;
    }
    console.log(reduce (items,(item)=>item))


  
  
  function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

for(let i=0;i<elements.length;i++)
{
  let a=cb(elements[i],i,elements)
  if(a!==undifined)
  {
    return a
  }
}
}
console.log(find (items,(item)=>item))


  function filter(elements,cb)
  {
    let ar=[];
    for(let i=0;i<elements.length;i++)
    {
      ar.push(elements[i])
    }
    return ar;
  }
  console.log(filter(items,(item)=>item))

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

    const stack = [...elements];
    const res = [];
    while(stack.length) {
      // pop value from stack
      const next = stack.pop();
      if(Array.isArray(next)) {
        // push back array items, won't modify the original input
        stack.push(...next);
      } else {
        res.push(next);
      }
    }
    // reverse to restore input order
    return res.reverse();
  }
  flatten(items);
  //